package com.example.rohjinsoo;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.view.View;

import com.example.rohjinsoo.databinding.ActivityMainBinding;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private static final float DISTANCE_MIN_DELTA = 500f;

    private static final int REQUEST_CODE_GPS_MODULE = 112;
    private static final int REQUEST_CODE_PERMISSION = 113;
    private static final int REQUEST_CODE_DETAIL_SETTING = 114;

    public static final String KEY_PLACE_API = "AIzaSyAz1a8gUa82VtC5nqxdvBIgHPgeeKL4EuI";

    private static final List<Place.Type> TYPES = Arrays.asList(new Place.Type[] {
            Place.Type.BAKERY,
            Place.Type.BAR,
            Place.Type.CAFE,
            Place.Type.FOOD,
            Place.Type.LIQUOR_STORE,
            Place.Type.MEAL_TAKEAWAY,
            Place.Type.MEAL_DELIVERY,
            Place.Type.RESTAURANT,
    });

    private ActivityMainBinding mActivityMainBinding = null;

    private ArrayList<PlaceWrapper> mPlaceWrappers = null;

    private LocationManager mLocationManager = null;

    private PlaceAdapter mAdapter = null;

    private static Handler mHandler = null;

    private Location mLocation = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
            Attach Layout to Code
         */
        mActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mActivityMainBinding.setMainActivity(this);

        /*
            Get Location Manager for GPS Sensing
         */
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        /*
            Declare array for Places
         */
        mPlaceWrappers = new ArrayList<>();

        /*
            Declare Handler for update AppCompatImageView

            Cause, only Main thread can touch widget
         */
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                HashMap<String, Object> map = (HashMap<String, Object>) msg.obj;

                /*
                    Draw Bitmap to View
                 */
                ((AppCompatImageView) map.get("view")).setImageBitmap((Bitmap) map.get("bitmap"));
            }
        };

        /*
            Declare Adapter
         */
        mAdapter = new PlaceAdapter() {

            /*
                Vibrate
             */
            @Override
            public void onClick(PlaceWrapper place) {
                Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibrator.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    vibrator.vibrate(500);
                }
            }
        };

        /*
            Attach Adapter to RecyclerView
         */
        mActivityMainBinding.recyclerView.setAdapter(mAdapter);
        mActivityMainBinding.recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, RecyclerView.VERTICAL, false));
    }

    /*
        Check and Request Permissions
     */
    private void checkPermissions() {
        if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) &&
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            /*
                if all granted, then check gps module
             */
            checkModule();
        } else {
            /*
                if there is no permission, request to user and get result by onRequestPermissionsResult method
             */
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION }, REQUEST_CODE_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CODE_PERMISSION:

                /*
                    check all permissions granted
                 */
                boolean allGranted = true;
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        allGranted = false;
                        break;
                    }
                }

                if (allGranted) {
                    /*
                        if all granted, then check gps module
                     */
                    checkModule();
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        // unreachable
                    } else {
                        /*
                            if not all granted, then ask to user and
                            turn on setting menu for permissions and
                            get result by onActivityResult
                         */
                        new AlertDialog
                                .Builder(this)
                                .setCancelable(false)
                                .setTitle("Permission setting")
                                .setMessage("Accept every permission")
                                .setPositiveButton("Setting", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        startActivityForResult(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                                .setData(Uri.fromParts("package", getPackageName(), null)), REQUEST_CODE_DETAIL_SETTING);
                                    }
                                })
                                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        System.exit(0);
                                    }
                                })
                                .create()
                                .show();
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_DETAIL_SETTING:
                /*
                    after close setting menu(permission)
                 */
                checkPermissions();
                break;

            case REQUEST_CODE_GPS_MODULE:
                /*
                    after close setting menu(gps)
                 */
                checkModule();
                break;
        }
    }

    @SuppressLint("MissingPermission")
    private void checkModule() {
        /*
            check each module enable
         */
        boolean isGpsEnable = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnable = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (isGpsEnable) {
            if (isNetworkEnable) {
                /*
                    register listener for location update by network(LTE)
                 */
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            }

            /*
                register listener for location update(GPS)
             */
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        } else {
            /*
                if all module disabled, then ask to user and
                get result by onActivityResult
             */
            new AlertDialog
                    .Builder(this)
                    .setCancelable(false)
                    .setTitle("GPS Setting")
                    .setMessage("Please turn on GPS to use the app smoothly.")
                    .setPositiveButton("Setting", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_CODE_GPS_MODULE);
                        }
                    })
                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            System.exit(0);
                        }
                    })
                    .create()
                    .show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        /*
            unregister listener when app closing
         */
        mLocationManager.removeUpdates(this);
    }

    /*
        invoked when location changed
     */
    @Override
    @SuppressLint("MissingPermission")
    public void onLocationChanged(@NonNull Location location) {
        if (mLocation != null) {
            /*
                if already has old location

                check if distance from old location to new location is larger then 500m
             */
            float delta = mLocation.distanceTo(location);

            if (delta > DISTANCE_MIN_DELTA) {
                /*
                    larger then 500m, continue
                 */
                mLocation = location;
            } else {
                /*
                    shorter then 500m, do nothing
                 */
                return;
            }
        } else {
            /*
                first app launch, continue
             */
            mLocation = location;
        }

        /*
            initialize Place SDK
         */
        Places.initialize(getApplicationContext(), KEY_PLACE_API);

        /*
            Create Place SDK Client
         */
        PlacesClient placesClient = Places.createClient(this);

        /*
            declare all valid needed properties

            https://developers.google.com/places/android-sdk/current-place#get-current
         */
        FindCurrentPlaceRequest findCurrentPlaceRequest = FindCurrentPlaceRequest.newInstance(Arrays.asList(new Place.Field[] {
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.PHOTO_METADATAS,

                Place.Field.BUSINESS_STATUS,
                Place.Field.ADDRESS,
                Place.Field.RATING,
                Place.Field.LAT_LNG,
                Place.Field.PRICE_LEVEL,
                Place.Field.USER_RATINGS_TOTAL,

                Place.Field.TYPES,
        }));

        /*
            query places to google
         */
        Task<FindCurrentPlaceResponse> findCurrentPlaceResponseTask = placesClient.findCurrentPlace(findCurrentPlaceRequest);

        /*
            get query result
         */
        findCurrentPlaceResponseTask.addOnCompleteListener(new OnCompleteListener<FindCurrentPlaceResponse>() {
            @Override
            public void onComplete(@NonNull Task<FindCurrentPlaceResponse> task) {
                if (task.isSuccessful()){
                    /*
                        remove all old places
                     */
                    mPlaceWrappers.clear();

                    FindCurrentPlaceResponse response = task.getResult();

                    /*
                        iterate results
                     */
                    for (PlaceLikelihood placeLikelihood : response.getPlaceLikelihoods()) {

                        /*
                            add only RESTAURANT like place to list
                         */
                        if (TYPES.contains(placeLikelihood.getPlace().getTypes().get(0))) {
                            mPlaceWrappers.add(new PlaceWrapper(placeLikelihood.getPlace()));
                        }
                    }

                    /*
                        update RecyclerView
                     */
                    mAdapter.update(mPlaceWrappers);
                } else {
                    /*
                        fail to find
                     */
                    Exception exception = task.getException();
                    if (exception instanceof ApiException) {
                        ApiException apiException = (ApiException) exception;
                    }
                }
            }
        });
    }

    public void onClickFind() {
        mActivityMainBinding.relativeLayout.setVisibility(View.GONE);
        mActivityMainBinding.recyclerView.setVisibility(View.VISIBLE);

        /*
            Start Sensing Process
         */
        checkPermissions();
    }

    /*
        Draw image to imageView from url
     */
    @BindingAdapter("app:imageUrl")
    public static void loadImageUrl(
            final AppCompatImageView imageView,
            final String imageUrl) {

        /*
            if image url is valid
         */
        if (!imageUrl.isEmpty()) {

            /*
                create thread
             */
            Executor executor = Executors.newSingleThreadExecutor();

            /*
                run
             */
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        /*
                            open inputStream
                         */
                        InputStream inputStream = new java.net.URL(imageUrl).openStream();

                        /*
                            get bytes from inputStream and save to bitmap
                         */
                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                        /*
                            close inputStream
                         */
                        inputStream.close();

                        /*
                            make parameters for handler
                         */
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("view", imageView);
                        map.put("bitmap", bitmap);

                        /*
                            update image view using handler
                         */
                        mHandler.obtainMessage(0, map).sendToTarget();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}

