package com.example.rohjinsoo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rohjinsoo.databinding.RecyclerViewItemPlaceBinding;

import java.util.ArrayList;

public abstract class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ViewHolder> {

    public abstract void onClick(PlaceWrapper place);

    private final ArrayList<PlaceWrapper> mPlaces;

    public void update(ArrayList<PlaceWrapper> places) {
        mPlaces.clear();
        mPlaces.addAll(places);
        notifyDataSetChanged();
    }

    /*
        ViewHolder describes view of each items
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        private final RecyclerViewItemPlaceBinding mBinding;

        public ViewHolder(@NonNull RecyclerViewItemPlaceBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public void bind(final PlaceWrapper place) {
            mBinding.setPlaceWrapper(place);

            mBinding.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PlaceAdapter.this.onClick(place);
                }
            });
        }
    }

    public PlaceAdapter() {
        mPlaces = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PlaceAdapter.ViewHolder(
                RecyclerViewItemPlaceBinding.inflate(
                        LayoutInflater.from(parent.getContext()),
                        parent,
                        false));
    }

    /*
        Put items in the order
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mPlaces.get(position));
    }

    @Override
    public int getItemCount() {
        return mPlaces.size();
    }
}
