package com.example.rohjinsoo;

import com.google.android.libraries.places.api.model.Place;

public class PlaceWrapper {

    private final Place mPlace;

    public PlaceWrapper(Place place) {
        mPlace = place;
    }

    /*
        Check whether place has photo
     */
    public boolean hasPhoto() {
        return mPlace.getPhotoMetadatas() != null;
    }

    /*
        get Photo Url
     */
    public String getPhoto() {
        return hasPhoto() ?
                "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + mPlace.getPhotoMetadatas().get(0).zza() + "&key=" + MainActivity.KEY_PLACE_API :
                "";
    }

    /*
        get Place Name
     */
    public String getName() {
        return (mPlace.getName() != null) ? mPlace.getName() : "";
    }

    /*
        get Place Address
     */
    public String getAddress() {
        return (mPlace.getAddress() != null) ? mPlace.getAddress() : "";
    }

    /*
        get Business Status
     */
    public String getBusinessStatus() {
        return (mPlace.getBusinessStatus() != null) ? (mPlace.getBusinessStatus().name().equals("OPERATIONAL") ? "Open" : mPlace.getBusinessStatus().name().equals("CLOSED_TEMPORARILY") ? "Temporal Close" : "Close") : "";
    }

    /*
        get Price Level($<chip> ~ $$$$$<expensive>)
     */
    public String getPriceLevel() {
        if (mPlace.getPriceLevel() != null) {
            switch (mPlace.getPriceLevel()) {
                case 0:
                    return "$";

                case 1:
                    return "$$";

                case 2:
                    return "$$$";

                case 3:
                    return "$$$$";

                case 4:
                default:
                    return "$$$$$";
            }
        } else {
            return "";
        }
    }

    /*
        check whether place has rating
     */
    public boolean hasRating() {
        return mPlace.getRating() != null;
    }

    /*
        get rating as string
     */
    public String getRatingString() {
        Double rating = mPlace.getRating();

        return (rating != null) ? String.format("%.1f", rating) : "";
    }

    /*
        get rating as float
     */
    public float getRatingFloat() {
        Double rating = mPlace.getRating();

        return (rating != null) ? rating.floatValue() : 0f;
    }

    /*
        get total user's ratings
     */
    public String getUserRatingsTotal() {
        Integer userRatingsTotal = mPlace.getUserRatingsTotal();

        if (userRatingsTotal != null) {
            return String.format("(%d)", userRatingsTotal);
        } else {
            return "";
        }
    }
}
